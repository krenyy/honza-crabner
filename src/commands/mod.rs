use crate::config::CONFIG;
use std::sync::Arc;
use twilight_http::Client as HttpClient;
use twilight_model::{
    application::{command::CommandType, interaction::InteractionData},
    gateway::payload::incoming::InteractionCreate,
    id::Id,
};
use twilight_util::builder::command::CommandBuilder;

macro_rules! generate_commands_match {
    (($cmd_data:expr, $http:expr, $interaction:expr), ($($cmd:ident),*)) => {
        match $cmd_data.name.as_str() {
            $(stringify!($cmd) => $cmd::execute($http, $interaction).await?,)*
            _ => ()
        }
    };
}

macro_rules! register_commands {
    ($($cmd:ident),+) => {
        $(
        mod $cmd;
        )*

        pub(crate) async fn register(http: &Arc<HttpClient>) -> anyhow::Result<()> {
            let guild = http
                .guild(Id::new(CONFIG.settings.guild))
                .exec()
                .await?
                .model()
                .await?;
            let interaction_client = http.interaction(
                http.current_user_application()
                    .exec()
                    .await?
                    .model()
                    .await?
                    .id,
            );
            interaction_client
                .set_guild_commands(
                    guild.id,
                    &[$(
                        CommandBuilder::new(
                            stringify!($cmd),
                            $cmd::DESCRIPTION.to_string(),
                            CommandType::ChatInput,
                        )
                        .validate()
                        .unwrap()
                        .build()
                    ),*],
                )
                .exec()
                .await?;

            Ok(())
        }

        pub(crate) async fn handle(
            http: Arc<HttpClient>,
            interaction: Box<InteractionCreate>,
        ) -> anyhow::Result<()> {
            match interaction.data.as_ref().unwrap() {
                InteractionData::ApplicationCommand(cmd_data) => {
                    generate_commands_match!((cmd_data, http, interaction), ($($cmd),*))
                }
                _ => (),
            }

            Ok(())
        }
    };
}

register_commands!(bot, boostcount);

use std::sync::Arc;
use twilight_http::Client as HttpClient;
use twilight_model::{
    channel::message::MessageFlags,
    gateway::payload::incoming::InteractionCreate,
    http::interaction::{InteractionResponse, InteractionResponseType},
};
use twilight_util::builder::InteractionResponseDataBuilder;

pub(crate) const DESCRIPTION: &str = "Get number of server boosts.";

pub(crate) async fn execute(
    http: Arc<HttpClient>,
    interaction: Box<InteractionCreate>,
) -> anyhow::Result<()> {
    let application = http
        .current_user_application()
        .exec()
        .await?
        .model()
        .await?;
    let interaction_client = http.interaction(application.id);
    let guild = http
        .guild(interaction.guild_id.unwrap())
        .exec()
        .await?
        .model()
        .await?;
    let premium_subscription_count = guild.premium_subscription_count.unwrap();

    interaction_client
        .create_response(
            interaction.id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(
                    InteractionResponseDataBuilder::new()
                        .flags(MessageFlags::EPHEMERAL)
                        .content(format!("{}", premium_subscription_count))
                        .build(),
                ),
            },
        )
        .exec()
        .await?;

    Ok(())
}

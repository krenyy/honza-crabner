use std::sync::Arc;
use twilight_http::Client as HttpClient;
use twilight_model::{
    application::component::{button::ButtonStyle, Button, Component},
    channel::message::MessageFlags,
    gateway::payload::incoming::InteractionCreate,
    http::interaction::{InteractionResponse, InteractionResponseType},
};
use twilight_util::builder::InteractionResponseDataBuilder;

pub(crate) const DESCRIPTION: &str = "Get info about bot. Version, source code, etc.";

pub(crate) async fn execute(
    http: Arc<HttpClient>,
    interaction: Box<InteractionCreate>,
) -> anyhow::Result<()> {
    let application = http
        .current_user_application()
        .exec()
        .await?
        .model()
        .await?;
    let interaction_client = http.interaction(application.id);

    interaction_client
        .create_response(
            interaction.id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(
                    InteractionResponseDataBuilder::new()
                        .flags(MessageFlags::EPHEMERAL)
                        .content(
                            "This bot is developed by the community.\n\
                            We will be happy if you join the development and help us further improve it.")
                        .build(),
                ),
            },
        )
        .exec()
        .await?;

    Ok(())
}

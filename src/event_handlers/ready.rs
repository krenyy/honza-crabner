use crate::commands;
use std::sync::Arc;
use twilight_http::Client as HttpClient;
use twilight_model::gateway::payload::incoming::Ready;

pub(crate) async fn handle(http: Arc<HttpClient>, _connected: Box<Ready>) -> anyhow::Result<()> {
    commands::register(&http).await?;

    Ok(())
}

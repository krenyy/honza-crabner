mod interaction_create;
mod message_create;
mod ready;
mod shard_connected;

use std::{error::Error, sync::Arc};
use twilight_gateway::Event;
use twilight_http::Client as HttpClient;

pub(crate) async fn handle_event(
    _shard_id: u64,
    event: Event,
    http: Arc<HttpClient>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    match event {
        Event::InteractionCreate(interaction) => {
            interaction_create::handle(http, interaction).await?
        }
        Event::MessageCreate(message) => message_create::handle(http, message).await?,
        Event::ShardConnected(connected) => shard_connected::handle(http, connected).await?,
        Event::Ready(ready) => ready::handle(http, ready).await?,
        // Other events here...
        _ => {}
    }

    Ok(())
}

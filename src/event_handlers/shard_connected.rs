use std::sync::Arc;
use tracing::info;
use twilight_http::Client as HttpClient;
use twilight_model::gateway::event::shard::Connected;

pub(crate) async fn handle(_http: Arc<HttpClient>, connected: Connected) -> anyhow::Result<()> {
    info!(
        "connected on shard {}, heartbeat interval: {}",
        connected.shard_id, connected.heartbeat_interval
    );

    Ok(())
}

use std::sync::Arc;
use tracing::{debug, info};
use twilight_http::Client as HttpClient;
use twilight_model::gateway::payload::incoming::MessageCreate;
use twilight_util::builder::embed::EmbedBuilder;

pub(crate) async fn handle(http: Arc<HttpClient>, msg: Box<MessageCreate>) -> anyhow::Result<()> {
    if msg.content.starts_with("::") {
        info!("sending message about `::` prefix removal");
        let channel = http
            .create_private_channel(msg.author.id)
            .exec()
            .await?
            .model()
            .await?;
        http.create_message(channel.id)
            .embeds(&[EmbedBuilder::new()
                .color(0xFF0000)
                .title("Error")
                .description("The `::` prefix was removed in favor of slash commands!")
                .build()])?
            .exec()
            .await?;
    }

    Ok(())
}

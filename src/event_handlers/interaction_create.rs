use crate::commands;
use std::sync::Arc;
use twilight_http::Client as HttpClient;
use twilight_model::gateway::payload::incoming::InteractionCreate;

pub(crate) async fn handle(
    http: Arc<HttpClient>,
    interaction: Box<InteractionCreate>,
) -> anyhow::Result<()> {
    commands::handle(http, interaction).await?;

    Ok(())
}

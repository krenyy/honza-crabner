mod commands;
mod config;
mod event_handlers;

use config::CONFIG;
use futures::stream::StreamExt;
use std::sync::Arc;
use twilight_cache_inmemory::{InMemoryCache, ResourceType};
use twilight_gateway::{cluster::ShardScheme, Cluster};
use twilight_http::Client as HttpClient;
use twilight_model::gateway::Intents;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    // TODO: specify exactly which intents are actually needed
    let intents = Intents::all();

    // we don't really need sharding for one guild
    let scheme = ShardScheme::Range {
        from: 0,
        to: 0,
        total: 1,
    };

    let (cluster, mut events) = Cluster::builder(CONFIG.secrets.discord.token.clone(), intents)
        .shard_scheme(scheme)
        .build()
        .await?;

    let cluster = Arc::new(cluster);

    // spin up the shards
    let cluster_spawn = Arc::clone(&cluster);
    tokio::spawn(async move {
        cluster_spawn.up().await;
    });

    // shut the shards down on ctrl-c
    let cluster_despawn = Arc::clone(&cluster);
    ctrlc::set_handler(move || cluster_despawn.down())?;

    let http = Arc::new(HttpClient::new(CONFIG.secrets.discord.token.clone()));
    let cache = InMemoryCache::builder()
        .resource_types(ResourceType::all())
        .build();

    while let Some((shard_id, event)) = events.next().await {
        cache.update(&event);
        tokio::spawn(event_handlers::handle_event(
            shard_id,
            event,
            Arc::clone(&http),
        ));
    }

    Ok(())
}

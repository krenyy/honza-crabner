use lazy_static::lazy_static;
use serde::Deserialize;
use std::{fs, num::NonZeroU64};

pub(crate) struct Config {
    pub(crate) settings: Settings,
    pub(crate) secrets: Secrets,
}

#[derive(Deserialize)]
pub(crate) struct Settings {
    pub(crate) guild: u64,
    pub(crate) roles: RoleIds,
}

#[derive(Deserialize)]
pub(crate) struct RoleIds {
    pub(crate) moderator: NonZeroU64,
}

#[derive(Deserialize)]
pub(crate) struct Secrets {
    pub(crate) cvut: CvutSecrets,
    pub(crate) discord: DiscordSecrets,
}

#[derive(Deserialize)]
pub(crate) struct CvutSecrets {
    pub(crate) client_id: String,
    pub(crate) client_secret: String,
}

#[derive(Deserialize)]
pub(crate) struct DiscordSecrets {
    pub(crate) token: String,
}

macro_rules! load_toml {
    ($config_file:expr) => {
        toml::from_str(
            &fs::read_to_string($config_file).expect(&format!("{} missing!", $config_file)),
        )
        .unwrap()
    };
}

pub(crate) fn load_config() -> Config {
    #[cfg(debug_assertions)]
    let settings = load_toml!("settings.dev.toml");

    #[cfg(not(debug_assertions))]
    let settings = load_toml!("settings.toml");

    let secrets = load_toml!("secrets.toml");

    Config { settings, secrets }
}

lazy_static! {
    /// Global immutable accessor for settings and secrets
    pub(crate) static ref CONFIG: Config = load_config();
}
